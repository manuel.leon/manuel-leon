﻿using APISmartwatch.Core.Base;
using APISmartwatch.Core.Entities;
using APISmartwatch.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace APISmartwatch.Business
{
    public class SmartwatchBusiness
    {
        private readonly EjecucionDao _dao;
        private readonly RespuestaModel m;

        public SmartwatchBusiness()
        {
            _dao = new EjecucionDao();
            m = new RespuestaModel();
        }

        public async Task<RespuestaModel> Get(Dictionary<string, dynamic> P)
        {
            var res = await _dao.QueryAsync<Smartwatch>(P, "[dbo].[GetSmartwatch]");
            m.Datos = res;
            return m;
        }

        public async Task<RespuestaModel> Set(Dictionary<string, dynamic> P)
        {
            return await _dao.QuerySingleAsync<RespuestaModel>(P, "[dbo].[SetSmartwatch]");
        }
    }
}