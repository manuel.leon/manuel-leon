using APISmartwatch.API;
using Swashbuckle.Application;
using System;
using System.IO;
using System.Reflection;
using System.Web.Http;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace APISmartwatch.API
{
    /// <summary>
    /// Configuracion del Swagger
    /// </summary>
    /// <returns></returns>
    public static class SwaggerConfig
    {
        private static string GetXmlCommentsPath()
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory + @"\bin\";
            var commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
            return Path.Combine(baseDirectory, commentsFileName);
        }

        /// <summary>
        /// Registro del Swagger
        /// </summary>
        /// <returns></returns>
        public static void Register()
        {
            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        // Use "SingleApiVersion" to describe a single version API. Swagger 2.0 includes an "Info" object to
                        // hold additional metadata for an API. Version and title are required but you can also provide
                        // additional fields by chaining methods off SingleApiVersion.
                        //
                        // DEFINIMOS LAS CARACTERÍSTICAS DEL WEB API.
                        c.SingleApiVersion("v1", "Heineken Smartwatch API");

                        // If you annotate Controllers and API Types with
                        // Xml comments (http://msdn.microsoft.com/en-us/library/b2s063f7(v=vs.110).aspx), you can incorporate
                        // those comments into the generated docs and UI. You can enable this by providing the path to one or
                        // more Xml comment files.
                        //
                        // HABILITAMOS EL ARCHIVO DE DOCUMENTACIÓN XML.
                        c.IncludeXmlComments(GetXmlCommentsPath());

                        // If you want the output Swagger docs to be indented properly, enable the "PrettyPrint" option.
                        //
                        c.PrettyPrint();
                    })
                .EnableSwaggerUi();
        }
    }
}