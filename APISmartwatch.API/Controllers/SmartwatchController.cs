﻿using APISmartwatch.API.Base;
using APISmartwatch.Business;
using APISmartwatch.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace APISmartwatch.API.Controllers
{
    /// <summary>
    /// API Para Smartwatch
    /// </summary>
    /// <returns></returns>
    [RoutePrefix("api/smartwatch")]
    public class SmartwatchController : BaseController
    {
        private readonly SmartwatchBusiness n;

        /// <summary>
        /// Constructor de la API.
        /// </summary>
        /// <returns></returns>
        public SmartwatchController()
        {
            n = new SmartwatchBusiness();
        }

        /// <summary>
        /// Obtienes informacion de los dispositivos.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("get")]
        public async Task<string> Get()
        {
            Dictionary<string, dynamic> param = new Dictionary<string, dynamic>();
            var res = await n.Get(param);

            return CustomJsonSerializer(res);
        }

        /// <summary>
        /// Envia la actividad del smartwatch
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("set")]
        public async Task<string> Set([FromBody]Smartwatch model)
        {
            Dictionary<string, dynamic> param = new Dictionary<string, dynamic>()
            {
                {"device",model.device},
                {"heart_rate",model.heart_rate},
                {"steps",model.steps},
                {"breathing_rpm",model.breathing_rpm},
                {"stress",model.stress},
                {"time", model.time }
            };
            var res = await n.Set(param);

            return CustomJsonSerializer(res);
        }
    }
}