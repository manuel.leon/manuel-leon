﻿using System.Web.Http;

namespace APISmartwatch.API
{
    /// <summary>
    /// Panel de Configuracion de la API
    /// </summary>
    /// <returns></returns>
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Metodo para iniciar la configuracion.
        /// </summary>
        /// <returns></returns>
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);            
        }
    }
}