﻿using Newtonsoft.Json;
using System.Web.Http;

namespace APISmartwatch.API.Base
{
    public class BaseController : ApiController
    {
        protected static string CustomJsonSerializer(object data)
        {
            return JsonConvert.SerializeObject(data, Formatting.None,
            new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }
    }
}