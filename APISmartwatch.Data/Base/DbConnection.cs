﻿using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace APISmartwatch.Data.Base
{
    public class DBConnection : Disposable
    {
        public string conexion { get; set; }

        public DBConnection()
        {
            conexion = ConfigurationManager.ConnectionStrings["Default"].ToString();
        }

        public DBConnection(string connection)
        {
            conexion = ConfigurationManager.ConnectionStrings[connection].ToString();
        }

        public IEnumerable<T> Query<T>(Dictionary<string, dynamic> P, string SP)
        {
            using (IDbConnection conn = new SqlConnection(conexion))
            {
                DynamicParameters DP = new DynamicParameters();
                if (P != null)
                {
                    foreach (KeyValuePair<string, dynamic> item in P)
                    {
                        DP.Add(item.Key, item.Value);
                    }
                }

                return conn.Query<T>(SP, param: DP, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(Dictionary<string, dynamic> P, string SP)
        {
            using (IDbConnection conn = new SqlConnection(conexion))
            {
                DynamicParameters DP = new DynamicParameters();
                if (P != null)
                {
                    foreach (KeyValuePair<string, dynamic> item in P)
                    {
                        DP.Add(item.Key, item.Value);
                    }
                }

                return await conn.QueryAsync<T>(SP, param: DP, commandType: CommandType.StoredProcedure);
            }
        }

        public T QuerySingle<T>(Dictionary<string, dynamic> P, string SP)
        {
            using (IDbConnection conn = new SqlConnection(conexion))
            {
                DynamicParameters DP = new DynamicParameters();

                foreach (KeyValuePair<string, dynamic> item in P)
                {
                    DP.Add(item.Key, item.Value);
                }
                return conn.QueryFirst<T>(SP, param: DP, commandType: CommandType.StoredProcedure);
            }
        }

        public T QuerySingleOrDefault<T>(Dictionary<string, dynamic> P, string SP)
        {
            using (IDbConnection conn = new SqlConnection(conexion))
            {
                DynamicParameters DP = new DynamicParameters();

                foreach (KeyValuePair<string, dynamic> item in P)
                {
                    DP.Add(item.Key, item.Value);
                }
                return conn.QueryFirstOrDefault<T>(SP, param: DP, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<T> QuerySingleAsync<T>(Dictionary<string, dynamic> P, string SP)
        {
            using (IDbConnection conn = new SqlConnection(conexion))
            {
                DynamicParameters DP = new DynamicParameters();

                foreach (KeyValuePair<string, dynamic> item in P)
                {
                    DP.Add(item.Key, item.Value);
                }
                return await conn.QueryFirstAsync<T>(SP, param: DP, commandType: CommandType.StoredProcedure);
            }
        }
    }
}