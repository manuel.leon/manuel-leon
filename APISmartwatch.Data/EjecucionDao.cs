﻿using APISmartwatch.Data.Base;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace APISmartwatch.Data
{
    public class EjecucionDao : Disposable
    {
        internal DBConnection _db;

        public EjecucionDao()
        {
            _db = new DBConnection(ConfigurationManager.AppSettings["conexion"]);
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(Dictionary<string, dynamic> P, string SP)
        {
            return await _db.QueryAsync<T>(P, SP);
        }

        public IEnumerable<T> Query<T>(Dictionary<string, dynamic> P, string SP)
        {
            return _db.Query<T>(P, SP);
        }

        public async Task<T> QuerySingleAsync<T>(Dictionary<string, dynamic> P, string SP)
        {
            return await _db.QuerySingleAsync<T>(P, SP);
        }

        public T QuerySingle<T>(Dictionary<string, dynamic> P, string SP)
        {
            return _db.QuerySingle<T>(P, SP);
        }

        public T QuerySingleOrDefault<T>(Dictionary<string, dynamic> P, string SP)
        {
            return _db.QuerySingleOrDefault<T>(P, SP);
        }

        protected override void DisposeCore()
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            Dispose();
        }
    }
}