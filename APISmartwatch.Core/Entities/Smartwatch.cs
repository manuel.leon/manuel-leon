﻿namespace APISmartwatch.Core.Entities
{
    public class Smartwatch
    {
        public string device { get; set; }
        public int heart_rate { get; set; }
        public int steps { get; set; }
        public int breathing_rpm { get; set; }
        public int stress { get; set; }
        public string time { get; set; }
    }
}